var express = require("express");
var cors= require("cors");
var app = express();

app.use(express.json());
app.use(cors());
app.listen(3000, () => console.log("servidor corriendo en el puerto 3000"));

var ciudades = [ "Montevideo", "Rio de Janeiro","Paris", "Barcelona", "Londres", "Miami"];
app.get("/ciudades", (req, res, next) => res.json(ciudades.filter((c) => c.toLowerCase().indexOf(req.query.q.toString().toLowerCase()) > -1) ));

var misDestinos = [];
app.get("/my", (req, res, next) => res.json(misDestinos));
app.post("/my", (req, res, next) => {
	console.log(req.body);
	//misDestinos = req.body;
	misDestinos.push(req.body.nuevo);
	res.json(misDestinos);
});

app.get("/api/translation", (req, res, next) => res.json([
		{ lang: req.query.lang, key: "hola", value: "Hola "+ req.query.lang}
	]));