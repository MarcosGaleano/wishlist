import { Component, OnInit, InjectionToken, Inject } from '@angular/core';
import { DestinosApiClient } from './../../models/destinos-api-client.model';
import { DestinoViaje } from './../../models/destino-viaje.model';
import { ActivatedRoute } from '@angular/router';
import { AppState } from '../../app.module';
import { Store } from '@ngrx/store';

class DestinosApiClientOld{
  getById(id: string) /*: DestinoViaje*/{
    console.log("old class");
    return null;
  }
}

interface AppConfig{ apiEndPoint: string};

const APP_CONFIG_VALUE: AppConfig = { apiEndPoint: "mi app.com"};

const APP_CONFIG = new InjectionToken<AppConfig>('app.config');

class DestinosApiClientDecorated extends DestinosApiClient {
  constructor(@Inject(APP_CONFIG) private config: AppConfig, store: Store<AppState>) {
    super(store);
    
    getById(id){
      console.log("clase decorada");
      console.log("config : "+ this.config.apiEndPoint);
      return super.getById(id);
    }
  }
}

@Component({
  selector: 'app-destino-detalle',
  templateUrl: './destino-detalle.component.html',
  styleUrls: ['./destino-detalle.component.css'],
  /*providers: [
            {provide: APP_CONFIG, useValue: APP_CONFIG_VALUE}, 
            {provide: DestinosApiClient, useClass: DestinosApiClientDecorated},
            {provide: DestinosApiClientOld, useExisting: DestinosApiClient}
            ]*/
  providers : [ DestinosApiClient ]
})
export class DestinoDetalleComponent implements OnInit {
  destino: DestinoViaje;
  style = {
    sources: {
      world: {
        type: 'geojson',
        data: 'https://raw.githubusercontent.com/johan/world.geo.json/master/countries.geo.json'
      }
    },
    version: 8,
    layers: [{
      'id': 'countries',
      'type': 'fill',
      'source': 'world',
      'layout': {},
      'paint': {
        'fill-color': '#6F788A'
      }
    }]
  };

  constructor(private route: ActivatedRoute, private destinosApiClient: DestinosApiClient) {}

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    this.destino = this.destinosApiClient.getById(id);
  }

}

