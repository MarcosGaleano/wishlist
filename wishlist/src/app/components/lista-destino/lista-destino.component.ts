import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DestinoViaje } from '../../models/destino-viaje.model';
import { DestinosApiClient } from '../../models/destinos-api-client.model';
import { AppState } from '../../app.module';
import { Store } from '@ngrx/store';
import { NuevoDestinoAction, ElegidoFavoritoAction } from '../../models/destinos-viajes-state.model';
 
@Component({
  selector: 'app-lista-destino',
  templateUrl: './lista-destino.component.html',
  styleUrls: ['./lista-destino.component.css'],
  providers: [ DestinosApiClient ]
})
export class ListaDestinoComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  updates: string[];
  all: any; 

  constructor(public destinosApiClient: DestinosApiClient, private store: Store<AppState>) {
  	this.onItemAdded= new EventEmitter();
    this.updates = [];
   
    this.store.select( state => state.destinos.favorito).subscribe(d=>{
      if(d != null )
        this.updates.push("Se ha elegido a "+ d.ciudad + " como favorito");
    });

    store.select(state => state.destinos.items).subscribe(items => this.all = items);
  }

  ngOnInit(): void {
  }

  agregado(d: DestinoViaje):void {
  	this.destinosApiClient.add(d);
    this.onItemAdded.emit(d);
    //this.store.dispatch(new NuevoDestinoAction(d));
  }

  elegido(d: DestinoViaje){
    //this.destinosApiClient.getAll().forEach(x => x.setSelected(false));
    //d.setSelected(true);
    this.destinosApiClient.elegir(d);
    //this.store.dispatch(new ElegidoFavoritoAction(d));
  }

}
