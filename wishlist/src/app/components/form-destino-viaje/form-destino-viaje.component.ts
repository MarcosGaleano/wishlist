import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DestinoViaje } from '../../models/destino-viaje.model';
import { FormGroup, FormBuilder, Validators, FormControl, ValidatorFn } from '@angular/forms';
import { fromEvent } from 'rxjs';
import { map, filter, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { ajax } from 'rxjs/ajax';
import { APP_CONFIG, AppConfig } from '../../app.module';
import { Injectable, Inject, forwardRef } from '@angular/core';


@Component({
  selector: 'app-form-destino-viaje',
  templateUrl: './form-destino-viaje.component.html',
  styleUrls: ['./form-destino-viaje.component.css']
})
export class FormDestinoViajeComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  fg: FormGroup;
  minLengthNombre: number = 3;
  searchResults!: string[];

  constructor(fb: FormBuilder, @Inject( forwardRef(()=>APP_CONFIG)) private config: AppConfig ){
  	this.onItemAdded = new EventEmitter();
  	this.fg = fb.group({
  		ciudad: ['', Validators.compose([
        Validators.required,
        this.nombreValidadorParametrizable(this.minLengthNombre)
        ])],
  		precio: ['']
  	});

  	// ejemplo de registo de observable
  	this.fg.valueChanges.subscribe((form: any) =>{
  		console.log(form);
  	});
  }

  ngOnInit(): void {
    let elemNombre = <HTMLInputElement>document.getElementById('nombre');
    fromEvent(elemNombre, 'input')
      .pipe(map((e: KeyboardEvent) => (e.target as HTMLInputElement).value),
        filter(text => text.length > 2),
        debounceTime(120),
        distinctUntilChanged(),
        switchMap((text: string) => ajax(this.config.apiEndpoint + '/ciudades?q=' + text)))
          .subscribe(ajaxResponse => this.searchResults = ajaxResponse.response);
  }

  guardar(ciudad: string, precio: string) : boolean {
  	let d= new DestinoViaje(ciudad, precio);
  	this.onItemAdded.emit(d);
  	return false;
  }

  nombreValidador(control: FormControl) : {[s:string] : boolean} | null{
    const len = control.value.toString().trim().length;

    if(len > 0 && len < 3)
      return { nombreInvalido: true};

    return null;
  }

  nombreValidadorParametrizable(minLength: number) /*: ValidatorFn*/ {
    return (control: FormControl): { [s: string]: boolean } | (null) => {
      const len = control.value.toString().trim().length;

      if( len > 0 && len < minLength)
        return {minLargoNombre: true};

      return null;
    }
  }

}
