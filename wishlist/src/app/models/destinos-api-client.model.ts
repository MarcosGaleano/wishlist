import { DestinoViaje } from './destino-viaje.model';
import { Subject, BehaviorSubject, ReplaySubject } from 'rxjs';
import { AppState} from '../app.module';
import { NuevoDestinoAction, ElegidoFavoritoAction } from './destinos-viajes-state.model';
import { Store } from '@ngrx/store';
import { HttpRequest, HttpHeaders, HttpClient, HttpEvent, HttpResponse, HttpClientModule } from '@angular/common/http';
import { Injectable, Inject, forwardRef } from '@angular/core';
import { StoreModule } from '@ngrx/store';

@Injectable(/*{
    providedIn: "root"
}*/)
export class DestinosApiClient {
  //destinos!:DestinoViaje[];
  //current: Subject<DestinoViaje> = new ReplaySubject<DestinoViaje>(1);

  constructor(private store: Store<AppState>, 
    @Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig,
    private http: HttpClient) {
       //this.destinos = [];
      this.store
      .select(state => state.destinos)
      .subscribe((data) => {
        console.log('destinos sub store');
        console.log(data);
        this.destinos = data.items;
      });
    this.store
      .subscribe((data) => {
        console.log('all store');
        console.log(data);
      });
  }

  add(d:DestinoViaje){
    const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN': 'token-seguridad'});
    const req = new HttpRequest('POST', this.config.apiEndpoint + '/my', { nuevo: d.nombre }, { headers: headers });
    this.http.request(req).subscribe((data: HttpResponse<{}>) => {
      if (data.status === 200) {
        this.store.dispatch(new NuevoDestinoAction(d));
        const myDb = db;
        myDb.destinos.add(d);
        console.log("todos los destinos de la base de datos");
        myDb.destinos.toArray().then(destinos => console.log(destinos))
      }
    });
  }

  elegir(d:DestinoViaje){
    this.store.dispatch(new ElegidoFavoritoAction(d));
  }

  getById(id: string): DestinoViaje {
    return this.destinos.filter(function(d) { return d.id.tostring() === id; })[0];
  }
}
